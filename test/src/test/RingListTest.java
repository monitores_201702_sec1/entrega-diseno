package test;

import model.data_structures.Ringlist;
import junit.framework.TestCase;

public class RingListTest extends TestCase 
{




	
		
		private Ringlist<Integer> ringlist;
		
		
			public void test01(){
				ringlist= new  Ringlist<>();
				ringlist.add(10);
				ringlist.add(2);
				ringlist.add(23);
				ringlist.add(15);
				String s= ringlist.toString();
				
				assertEquals(Integer.valueOf(4), ringlist.getSize());
				assertEquals("[15 23 2 10]", s);
			}
			
			public void test02(){
				ringlist= new  Ringlist<>();
				ringlist.AddAtEnd(10);
				ringlist.AddAtEnd(2);
				ringlist.AddAtEnd(23);
				ringlist.AddAtEnd(15);
				String s= ringlist.toString();
				
				assertEquals(Integer.valueOf(4), ringlist.getSize());
				assertEquals("[10 2 23 15]", s);
				
			}
			
			public void test03(){
				ringlist= new  Ringlist<>();
				ringlist.AddAtEnd(10);
				ringlist.AddAtEnd(2);
				ringlist.AddAtEnd(23);
				ringlist.AddAtEnd(15);
				ringlist.addAtk(34, 3);
				ringlist.addAtk(33, 5);
				ringlist.addAtk(1, 1);
				ringlist.addAtk(99, 7);
				ringlist.addAtk(5, 10);
				String s= ringlist.toString();
				
				assertEquals(Integer.valueOf(9), ringlist.getSize());
				assertEquals("[1 10 2 34 23 15 33 99 5]", s);
			}
			
			public void test04(){
				ringlist= new  Ringlist<>();
				ringlist.add(10);
				ringlist.add(2);
				ringlist.add(23);
				ringlist.add(15);
				
				
				assertEquals(Integer.valueOf(15), ringlist.getElement(1));
				assertEquals(Integer.valueOf(23), ringlist.getElement(2));
				assertEquals(Integer.valueOf(2), ringlist.getElement(3));
				assertEquals(Integer.valueOf(10), ringlist.getElement(4));
			}
			
			
			public void test06(){
				ringlist = new Ringlist<>();
				ringlist.add(10);
				ringlist.add(2);
				ringlist.add(23);
				ringlist.add(15);
				ringlist.deleteAtk(3);
				
				String s = ringlist.toString();
				
				assertEquals("[15 23 10]", s);
				
			}
			
			

	}

