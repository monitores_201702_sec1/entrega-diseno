package test;

import java.util.NoSuchElementException;

import junit.framework.TestCase;
import model.data_structures.MaxPQ;

public class MaxPQTest extends TestCase
{
	
	private MaxPQ<Integer> colaMax;
	public void setupEscenario1()
	{
		colaMax= new MaxPQ<>();
	}
	
	
	public void setupEscenario2()
	{
		colaMax= new MaxPQ<>();
		colaMax.insert(10);
		colaMax.insert(7);
		colaMax.insert(9);
		colaMax.insert(3);
	
	}
	
	
	
	
    /**
     * Returns true if this priority queue is empty.
     *
     * @return {@code true} if this priority queue is empty;
     *         {@code false} otherwise
     */
    public void testisEmpty() {
       
    	setupEscenario1();
    	assertTrue(colaMax.isEmpty());
    }

    /**
     * Returns the number of keys on this priority queue.
     *
     * @return the number of keys on this priority queue
     */
    public void testsize() {
    	setupEscenario2();
        assertEquals(4, colaMax.size());
    }

    /**
     * Returns a largest key on this priority queue.
     *
     * @return a largest key on this priority queue
     * @throws NoSuchElementException if this priority queue is empty
     */
    public void testmax() 
    {
        setupEscenario2();
        assertTrue(10== colaMax.max());
    }



    /**
     * Adds a new key to this priority queue.
     *
     * @param  x the new key to add to this priority queue
     */
    public void testinsert() {

       setupEscenario1();
       colaMax.insert(17);
       assertEquals(false, colaMax.isEmpty());
    
    
    
    }

    
    public void testdelMax() {
     
    setupEscenario2();
    colaMax.delMax();
    assertTrue(9==colaMax.max());
   }
}
