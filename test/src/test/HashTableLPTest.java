package test;

import junit.framework.TestCase;
import model.data_structures.HashTableLP;

public class HashTableLPTest extends TestCase
{
	private HashTableLP<Integer, String> hashLP;

	public void setUpEscenario1()
	{

		hashLP= new HashTableLP<>(15);

	}

	public void setUpEscenario2()
	{
		hashLP= new HashTableLP<>(15);
		hashLP.put(1, "Prueba 1");
		hashLP.put(2, "Prueba 2");
	}
	public void testHashTableLP()
	{
		//		  Verifica si se crea la hashTable con el numero primo m�s cercano al ingresado por parametro.
		setUpEscenario1();
		assertEquals(17, hashLP.tama�o(), 0.01);

	}


	public void testsize() {
		setUpEscenario2();
		assertEquals(2, hashLP.size());
	}


	public void testisEmpty() {

		setUpEscenario1();
		assertTrue(hashLP.isEmpty());
	}

	public void testcontains() {

		setUpEscenario2();
		assertTrue(hashLP.contains(2));
		assertFalse(hashLP.contains(5));

	}



	/**
	 * Inserts the specified key-value pair into the symbol table, overwriting the old 
	 * value with the new value if the symbol table already contains the specified key.
	 * Deletes the specified key (and its associated value) from this symbol table
	 * if the specified value is {@code null}.
	 *
	 * @param  key the key
	 * @param  val the value
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public void testput() 
	{
		setUpEscenario1();
		hashLP.put(10, "Ke onda ke pez");
		assertTrue(hashLP.contains(10));


	}


		    /**
		     * Returns the value associated with the specified key.
		     * @param key the key
		     * @return the value associated with {@code key};
		     *         {@code null} if no such value
		     * @throws IllegalArgumentException if {@code key} is {@code null}
		     */
		    public void testget() 
		    {
		      setUpEscenario2();
		      assertEquals("Prueba 1", hashLP.get(1));
		    }
	
		    
		    /**
		     * Removes the specified key and its associated value from this symbol table     
		     * (if the key is in this symbol table).    
		     *
		     * @param  key the key
		     * @throws IllegalArgumentException if {@code key} is {@code null}
		     */
		    public void testdelete() 
		    {
		        setUpEscenario2();
		        hashLP.delete(2);
		        assertFalse(hashLP.contains(2));
		       
		    }
	
	
}
