package test;

import java.util.NoSuchElementException;

import junit.framework.TestCase;
import model.data_structures.BST;

public class BSTTest extends TestCase
{
	private BST<Integer, String> bst;
	
	public void setUpEscenario1()
	{
		bst = new BST<>();
	}
	
	
	public void setUpEscenario2()
	{
		bst = new BST<>();
		bst.put(1, "A");
		bst.put(2, "B");
		bst.put(3, "C");
		bst.put(4, "D");
		bst.put(5, "E");
		bst.put(6, "F");
		bst.put(7, "G");
		bst.put(8, "H");
		bst.put(9, "I");
		bst.put(10, "J");
		bst.put(11, "K");
		bst.put(12, "L");
		
	}
	
	
	
	/**
     * Returns the value associated with the given key.
     *
     * @param  key the key
     * @return the value associated with the given key if the key is in the symbol table
     *         and {@code null} if the key is not in the symbol table
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void testget() {
        setUpEscenario2();
        assertEquals("G", bst.get(7));
    }


    /**
     * Inserts the specified key-value pair into the symbol table, overwriting the old 
     * value with the new value if the symbol table already contains the specified key.
     * Deletes the specified key (and its associated value) from this symbol table
     * if the specified value is {@code null}.
     *
     * @param  key the key
     * @param  val the value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void testput() {
     setUpEscenario1();
     bst.put(2, "B");
    }


    /**
     * Removes the smallest key and associated value from the symbol table.
     *
     * @throws NoSuchElementException if the symbol table is empty
     */
    public void testdeleteMin() {
  
    	setUpEscenario2();
    	bst.deleteMin();
    	assertFalse(bst.contains(1));
    }

    /**
     * Removes the largest key and associated value from the symbol table.
     *
     * @throws NoSuchElementException if the symbol table is empty
     */
    public void deleteMax() {
    	setUpEscenario2();
    	bst.deleteMax();
    	assertFalse(bst.contains(12));
    }

    /**
     * Removes the specified key and its associated value from this symbol table     
     * (if the key is in this symbol table).    
     *
     * @param  key the key
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void delete() {
 
    	setUpEscenario2();
    	bst.delete(10);
    assertFalse(bst.contains(10));
    } 


   
}
