package test;

import junit.framework.TestCase;
import model.data_structures.HashTableLP;
import model.data_structures.HashTableSC;

public class HashTableSCTest extends TestCase
{

	private HashTableSC<Integer, String> HashSC;

	public void setUpEscenario1()
	{

		HashSC= new HashTableSC<>(12);

	}

	public void setUpEscenario2()
	{
		HashSC= new HashTableSC<>(15);
		HashSC.put(1, "Prueba 1");
		HashSC.put(2, "Prueba 2");
	}
	public void testHashTableLP()
	{
		//		  Verifica si se crea la hashTable con el numero primo m�s cercano al ingresado por parametro.
		setUpEscenario1();
		assertEquals(13, HashSC.tama�o(), 0.01);

	}


	public void testsize() {
		setUpEscenario2();
		assertEquals(2, HashSC.size());
	}


	public void testisEmpty() {

		setUpEscenario1();
		assertTrue(HashSC.isEmpty());
	}

	public void testcontains() {

		setUpEscenario2();
		assertTrue(HashSC.contains(2));
		assertFalse(HashSC.contains(5));

	}



	/**
	 * Inserts the specified key-value pair into the symbol table, overwriting the old 
	 * value with the new value if the symbol table already contains the specified key.
	 * Deletes the specified key (and its associated value) from this symbol table
	 * if the specified value is {@code null}.
	 *
	 * @param  key the key
	 * @param  val the value
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public void testput() 
	{
		setUpEscenario1();
		HashSC.put(10, "Ke onda ke pez");
		assertTrue(HashSC.contains(10));


	}


		    /**
		     * Returns the value associated with the specified key.
		     * @param key the key
		     * @return the value associated with {@code key};
		     *         {@code null} if no such value
		     * @throws IllegalArgumentException if {@code key} is {@code null}
		     */
		    public void testget() 
		    {
		      setUpEscenario2();
		      assertEquals("Prueba 1", HashSC.get(1));
		    }
	
	
	
}
