package test;

import java.util.NoSuchElementException;

import junit.framework.TestCase;
import model.data_structures.MinPQ;

public class MinPQTest extends TestCase
{
	private MinPQ<Integer> colaMin;
	public void setupEscenario1()
	{
		colaMin= new MinPQ<>();
	}
	
	
	public void setupEscenario2()
	{
		colaMin= new MinPQ<>();
		colaMin.insert(10);
		colaMin.insert(7);
		colaMin.insert(9);
		colaMin.insert(3);
	
	}
	
	
	
	
    /**
     * Returns true if this priority queue is empty.
     *
     * @return {@code true} if this priority queue is empty;
     *         {@code false} otherwise
     */
    public void testisEmpty() {
       
    	setupEscenario1();
    	assertTrue(colaMin.isEmpty());
    }

    /**
     * Returns the number of keys on this priority queue.
     *
     * @return the number of keys on this priority queue
     */
    public void testsize() {
    	setupEscenario2();
        assertEquals(4, colaMin.size());
    }

    /**
     * Returns a largest key on this priority queue.
     *
     * @return a largest key on this priority queue
     * @throws NoSuchElementException if this priority queue is empty
     */
    public void testmin() 
    {
        setupEscenario2();
        assertTrue(3== colaMin.min());
    }



    /**
     * Adds a new key to this priority queue.
     *
     * @param  x the new key to add to this priority queue
     */
    public void testinsert() {

       setupEscenario1();
       colaMin.insert(17);
       assertEquals(false, colaMin.isEmpty());
    
    
    
    }

    
    public void testdelmin() {
     
    setupEscenario2();
    colaMin.delMin();
    assertTrue(7==colaMin.min());
   }
}

