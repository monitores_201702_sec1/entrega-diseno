package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T>{

	Integer getSize();
	
	public void add(T elemento);
	public void AddAtEnd(T elemento);
	public void addAtk(T elemento , int k);
	public T getElement(int i);
	public T getCurrentElement();
	public T delete(T elemento);
	public T deleteAtk(int k);
	public T next();
	public T previous();

	
	
	
	
	


}
