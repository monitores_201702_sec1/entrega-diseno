package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList<T> implements IList<T>{

	private Node<T> primero;
	private Node<T> ultimo;
	private Node<T> actual;
	private int size;
	
	public DoubleLinkedList(){
		primero=null;
		ultimo=null;
		actual=null;
		size=0;
	}
	
	
	@Override
	public Iterator<T> iterator() {
		
		return new Iterator<T>() {

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
		};
	}

	@Override
	public Integer getSize() {
		return size;
	}

	@Override
	public void add(T pElemento) {
		Node nodo= new Node<>();
		nodo.agregarElemento(pElemento);
		if(primero==null){
			actual=nodo;
			primero= nodo;
			ultimo= nodo;
			size++;
		}
		else{
			nodo.cambiarSiguiente(primero);
			primero.cambiarAnterior(nodo);
			primero=nodo;
			actual=nodo;
			size++;
		}
		
			
	
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void AddAtEnd(T pElemento) {
		Node nodo= new Node<>();
		

		

		nodo.agregarElemento(pElemento);
				

		if(primero==null){
			primero=   nodo;
			ultimo= nodo;
			size++;
		}
		else{
			ultimo.cambiarSiguiente( nodo);
			nodo.cambiarAnterior(ultimo);
			ultimo= nodo;
			size++;
		}
		
	}

	@Override
	public void addAtk(T pElemento, int k) {
		Node<T> nodo= new Node<T>();
		nodo.agregarElemento(pElemento);
				
		if(primero==null||k==1){
			add(pElemento);
		}else if(k>=getSize()){
			AddAtEnd(pElemento);
		}
		else if(k<getSize()/2){
			actual=primero;
			boolean termino= false;
			int posicion= 1;
			while(actual!=null&& !termino){
				if(posicion==k){
					nodo.cambiarAnterior(actual.darAnterior());
					nodo.cambiarSiguiente(actual);
					actual.darAnterior().cambiarSiguiente(nodo);
					actual.cambiarAnterior(nodo);
					termino= true;
					size++;
				}
				actual=actual.darSiguiente();
				posicion++;
			}
		}
		else{
			actual=ultimo;
			boolean termino= false;
			int posicion= getSize();
			while(actual!=null&& !termino){
				if(posicion==k){
					nodo.cambiarAnterior(actual.darAnterior());
					nodo.cambiarSiguiente(actual);
					actual.darAnterior().cambiarSiguiente(nodo);
					actual.cambiarAnterior(nodo);
					termino= true;
					size++;
				}
				actual=actual.darAnterior();
				posicion--;
			}
		}
		
	}

	@Override
	public T getElement(int i) {
		 Node p = null;
	        if (i < getSize() / 2) {
	            p = primero;
	            for (int j = 1; j < i; j++)
	                p = p.darSiguiente();
	        } else {
	            p = ultimo;
	            for (int j = getSize(); j > i; j--)
	                p = p.darAnterior();
	        }
	        return (T) p.darElemento();
	    }
		
		
	

	@Override
	public T getCurrentElement() {
	
		return actual.darElemento();
	}

	@Override
	public T delete(T elemento) {
		T eliminado= null;
		if(getSize()>0){
			
			actual=primero;
			boolean termino= false;
			while(actual!=null||!termino){
				if(actual.darElemento()==elemento&& actual==primero){
					eliminado=(T) primero;
					primero= primero.darSiguiente();
					primero.darAnterior().cambiarSiguiente(null);
					primero.cambiarAnterior(null);
					termino=true;
					
				}
				else if(actual.darElemento()==elemento&& actual==ultimo){
					eliminado=(T) ultimo;
					ultimo= ultimo.darAnterior();
					ultimo.darSiguiente().cambiarAnterior(null);
					ultimo.cambiarSiguiente(null);
					termino=true;
				}
				else if(actual.darElemento()==elemento){
					eliminado=(T) actual;
					actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
					actual.darSiguiente().cambiarAnterior(actual.darAnterior());
					actual.cambiarAnterior(null);
					actual.cambiarSiguiente(null);
					termino=true;
				}
				actual=actual.darSiguiente();
			}
		}
		return eliminado;
		
	}

	@Override
	public T deleteAtk(int k) {
		T eliminado= null;
		if(getSize()>0){
			int posicion=1;
			if(k==1){
				eliminado=(T) primero;
				primero= primero.darSiguiente();
				primero.darAnterior().cambiarSiguiente(null);
				primero.cambiarAnterior(null);				
			}	
			else if(k==getSize()){
				eliminado=(T) ultimo;
				ultimo= ultimo.darAnterior();
				ultimo.darSiguiente().cambiarAnterior(null);
				ultimo.cambiarSiguiente(null);
			}
			else{
				if(k<getSize()/2){
					posicion=2;
					actual=primero;
					boolean termino=false;
					while(actual!=null&& !termino){
						if(k==posicion){
							eliminado=(T) actual;
							actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
							actual.darSiguiente().cambiarAnterior(actual.darAnterior());
							actual.cambiarAnterior(null);
							actual.cambiarSiguiente(null);
							termino=true;
						}
						posicion++;
						actual=actual.darSiguiente();
					}
				}
				else{
					posicion=getSize();
					actual=ultimo;
					boolean termino=false;
					while(actual!=null&& !termino){
						if(k==posicion){
							eliminado=(T) actual;
							actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
							actual.darSiguiente().cambiarAnterior(actual.darAnterior());
							actual.cambiarAnterior(null);
							actual.cambiarSiguiente(null);
							termino=true;
						}
						posicion--;
						actual=actual.darAnterior();
					
				}
			}
			}
			
		}
		return eliminado;
		
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		return (T) actual.darSiguiente();
	}

	@Override
	public T previous() {
		// TODO Auto-generated method stub
		return (T) actual.darAnterior();
	}

	 public String toString(){
	        String retStr = "[";

	        actual = primero;
	        while(actual != null){
	        	if(actual.darSiguiente()!=null){
	            retStr += actual.darElemento() + " ";
	            actual = actual.darSiguiente();
	        	}
	        	else{
	        		retStr += actual.darElemento();
		            actual = actual.darSiguiente();
	        	}
	        }

	        return retStr+"]";
	    }
	
	 public T first()
	 {
		 return (T) primero;
	 }

}
