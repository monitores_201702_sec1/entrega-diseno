package model.data_structures;

import java.util.Iterator;

public class Ringlist<T> implements IList<T> {

	private Node<T> primero;
	private Node<T> ultimo;
	private Node<T> actual;
	private int size;
	
	public Ringlist()
	{
		primero=null;
		ultimo=null;
		actual=null;
		size=0;
	}
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() {

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
		};	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void add(T elemento) {

		if(primero==null)
		{
			Node<T> nodo = new Node<T>();
			nodo.agregarElemento(elemento);
			primero=nodo;// TODO Auto-generated method stub
		ultimo=nodo;
		primero.cambiarSiguiente(ultimo);
		primero.cambiarAnterior(ultimo);
		ultimo.cambiarSiguiente(primero);
		ultimo.cambiarAnterior(primero);
		size++;
		}
		else
		{
			Node<T> nodoActual = new Node<T>();
			nodoActual.agregarElemento(elemento);
			
			nodoActual.cambiarSiguiente(primero);
			nodoActual.cambiarAnterior(ultimo);
			primero.cambiarAnterior(nodoActual);
			primero=nodoActual;
			ultimo.cambiarSiguiente(primero);
			size++;
		}
		
	}

	@Override
	public void AddAtEnd(T elemento) {
		// TODO Auto-generated method stub

		if(primero==null)
		{
			Node<T> nodo = new Node<T>();
			nodo.agregarElemento(elemento);
			primero=nodo;// TODO Auto-generated method stub
		ultimo=nodo;
		actual=nodo;
		primero.cambiarSiguiente(ultimo);
		primero.cambiarAnterior(ultimo);
		ultimo.cambiarSiguiente(primero);
		ultimo.cambiarAnterior(primero);
		size++;
		}
		else
		{

			Node<T> nodoActual = new Node<T>();
			nodoActual.agregarElemento(elemento);
			nodoActual.cambiarSiguiente(primero);
			nodoActual.cambiarAnterior(ultimo);
			primero.cambiarAnterior(nodoActual);
			ultimo.cambiarSiguiente(nodoActual);
			ultimo=nodoActual;
			actual=nodoActual;
			size++;
		}
	}



	public void addAtk(T pElemento, int k) {

		// TODO Auto-generated method stub
		Node<T> nodo= new Node<T>();
		nodo.agregarElemento(pElemento);
				
		if(primero==null||k==1){
			add(pElemento);
		}else if(k>=getSize()){
			AddAtEnd(pElemento);
		}
		else if(k<getSize()/2){
			actual=primero;
			boolean termino= false;
			int posicion= 1;
			while(actual!=null&& !termino){
				if(posicion==k){
					nodo.cambiarAnterior(actual.darAnterior());
					nodo.cambiarSiguiente(actual);
					actual.darAnterior().cambiarSiguiente(nodo);
					actual.cambiarAnterior(nodo);
					termino= true;
					size++;
				}
				actual=actual.darSiguiente();
				posicion++;
			}
		}
		else{
			actual=ultimo;
			boolean termino= false;
			int posicion= getSize();
			while(actual!=null&& !termino){
				if(posicion==k){
					nodo.cambiarAnterior(actual.darAnterior());
					nodo.cambiarSiguiente(actual);
					actual.darAnterior().cambiarSiguiente(nodo);
					actual.cambiarAnterior(nodo);
					termino= true;
					size++;
				}
				actual=actual.darAnterior();
				posicion--;
			}
		}
		
	}



	public T getElement(int i) {

		// TODO Auto-generated method stub
		if(i>size)
		i= i-size;
		 Node<T> p = null;

		  if (i < getSize() / 2) {
	            p = primero;
	            for (int j = 1; j < i; j++)
	                p = p.darSiguiente();
	        } else {
	            p = ultimo;
	            for (int j = getSize(); j > i; j--)
	                p = p.darAnterior();
	        }
	        return (T) p.darElemento();
	    }
			
		

	@Override
	public T getCurrentElement() {
		// TODO Auto-generated method stub
		return actual.darElemento();
	}

	@Override
	public T delete(T elemento) 
	{
		boolean termino=false;
		actual = primero;
		int contador=0;
		while(!termino && contador<size)
		{
			System.out.println("while");
			T elementoActual = actual.darElemento();
			if(elementoActual == elemento)
			{ 
				if(actual==primero)
				{
					actual=primero;
					ultimo.cambiarSiguiente(primero.darSiguiente());
					primero=primero.darSiguiente();
					primero.cambiarAnterior(ultimo);
					termino=true;
					System.out.println("entra al primero");
				}
				
				else if(actual==ultimo)
				{
					actual=ultimo;
					primero.cambiarAnterior(ultimo.darAnterior());
					ultimo=ultimo.darAnterior();
					ultimo.cambiarSiguiente(primero);
					System.out.println("entra al ulyimo");
					termino=true;
				}
				
				else
				{
					actual.darSiguiente().cambiarAnterior(actual.darAnterior());
					actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
					termino=true;
					System.out.println("Mitad");
				}
				actual=actual.darSiguiente();
				
			}
			contador++;
		}
		if(termino)
		return actual.darElemento();
		
		else
			return null;
		
		
	}

	@Override

	

	public T deleteAtk(int k) 
	{
		if (k>size)
			k=k-size;
		if(getSize()>0){
			int posicion=1;
			if(k==1)
			{
				ultimo.cambiarSiguiente(primero.darSiguiente());
				primero= primero.darSiguiente();
				
				primero.cambiarAnterior(ultimo);				
			}	
			else if(k==getSize()){
				primero.cambiarAnterior(ultimo.darAnterior());
				ultimo= ultimo.darAnterior();
				ultimo.cambiarSiguiente(primero);
			}
			else{
				if(k<getSize()/2){
					posicion=2;
					actual=primero;
					boolean termino=false;
					while(actual!=null&& !termino){
						if(k==posicion){
							actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
							actual.darSiguiente().cambiarAnterior(actual.darAnterior());
							
							termino=true;
						}
						posicion++;
						actual=actual.darSiguiente();
					}
				}
				else{
					posicion=getSize();
					actual=ultimo;
					boolean termino=false;
					while(actual!=null&& !termino){
						if(k==posicion){
							actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
							actual.darSiguiente().cambiarAnterior(actual.darAnterior());
							
							termino=true;
						}
						posicion--;
						actual=actual.darAnterior();
					
				}
			}
			}


		}	

		

			

		
		return (T) actual;

		}		

	
		
	@Override
	public T next() {
		// TODO Auto-generated method stub
		return (T) actual.darSiguiente();
	}

	@Override
	public T previous() {
		// TODO Auto-generated method stub
		return (T) actual.darAnterior();
	}
	
	public T first()
	{
		return (T) primero;
	}

	public T last()
	{
		return (T) ultimo;
	}
	
	 public String toString(){
	        String retStr = "[";

	        actual = primero;
	        while(actual != ultimo){
	        	
	            retStr += actual.darElemento() + " ";
	            actual = actual.darSiguiente();
	        	
	        	
	          
	        }
	        retStr += ultimo.darElemento();

	        return retStr+ "]";
	    }
}
