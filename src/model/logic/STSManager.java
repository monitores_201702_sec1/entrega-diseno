package model.logic;

import java.io.BufferedReader;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ISTSManager;
import model.vo.BusUpdateVO;
import model.vo.VOAgency;
import model.vo.VORoute;
import model.vo.VOStopTimes;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.HashTableLP;
import model.data_structures.IList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Ringlist;

public class STSManager<T> implements ISTSManager {
	
	private DoubleLinkedList<T> routesList;
	private DoubleLinkedList<T> stopTimes;
	private DoubleLinkedList<T> agencies;
	private HashTableLP<Integer, VOTrip> trips;
	private IQueue<BusUpdateVO> actualizaciones;
	HashTableLP<Integer,VOStop> stops= new HashTableLP<>();
	
	
	private final static String SEPARADOR= ",";

	@Override
	public void loadRoutes(String routesFile) {
		
		routesList= new DoubleLinkedList<T>();
		try {
			FileReader fr= new FileReader(new File(routesFile));
			BufferedReader bf = new BufferedReader(fr);
			
			String linea = bf.readLine();
			linea= bf.readLine();
			
			while(linea!=null){
				
				String[] datos=linea.split(SEPARADOR);
				
				VORoute ruta = new VORoute(Integer.parseInt(datos[0]), datos[1], datos[2], datos[3], datos[4], Integer.parseInt(datos[5]), datos[6], datos[6], datos[7]);
				routesList.AddAtEnd((T) ruta);
				
				linea= bf.readLine();
			}
			
			bf.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
	}

	
public void loadAgencys(String agencyFiles)
{
	agencies= new DoubleLinkedList<T>();
	
	try {
		FileReader fr = new FileReader(new File(agencyFiles));
		BufferedReader bf = new BufferedReader(fr);
		
		
		String linea =bf.readLine();
		linea= bf.readLine();
		
		while(linea!=null){
			
			String[] datos=linea.split(SEPARADOR);
			
			VOAgency agencia = new VOAgency(datos[0], datos[1], datos[2], datos[3], datos[4]);
			agencies.AddAtEnd((T) agencia);
			
			linea= bf.readLine();
		}
		
		
		
	} catch (IOException e) 
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
		
}


	/**
	* Se carga la informacion de los viajes por cada ruta, se tiene un lista cirular con rutas, dentro de las cuales se tienen listas circulares con los respectivos viajes.
	* No se ordenan los datos. Se espera mejorar para el proyecto
	*/
	public void loadTrips(String tripsFile) {
		
	trips= new HashTableLP<>();
	
		
		try {
	
				FileReader fr = new FileReader(new File(tripsFile));
				BufferedReader bf= new BufferedReader(fr);
				String linea = bf.readLine();
				linea= bf.readLine();
		
			while(linea!=null){
				
				String[] datos= linea.split(SEPARADOR,10);
					
					VOTrip trip= new VOTrip(Integer.parseInt(datos[0]), Integer.parseInt(datos[1]), Integer.parseInt(datos[2]),datos[3] , datos[4], Integer.parseInt(datos[5]), Integer.parseInt(datos[6]), Integer.parseInt(datos[7]),Integer.parseInt(datos[8]),Integer.parseInt(datos[9]));
				trips.put(trip.tripID(), trip);               
			

				
				linea=bf.readLine();
			}
			

			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	@Override
	public void loadStopTimes(String stopTimesFile) {
		stopTimes= new DoubleLinkedList<T>();
		
		try {
			FileReader fr = new FileReader(new File(stopTimesFile));
			BufferedReader bf= new BufferedReader(fr);
			String linea = bf.readLine();
			linea= bf.readLine();
			
			while(linea!=null){
				String[] datos= linea.split(SEPARADOR,9);
				
				int numero;
				double numero2;
				if(datos[5].equals("")){
					numero=0;
				}else numero=Integer.parseInt(datos[6]);
				
				if(datos[8].equals("")){
					numero2=0;
				}else numero2=Double.parseDouble(datos[8]);
				
				VOStopTimes stopTime= new VOStopTimes(Integer.parseInt(datos[0]), datos[1], datos[2],Integer.parseInt(datos[3]) , Integer.parseInt(datos[4]),numero , Integer.parseInt(datos[6]), Integer.parseInt(datos[7]),numero2);
			 
				
				linea=bf.readLine();
			}
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void loadStops(String stopsFile) {
		
		
		try {
			FileReader fr = new FileReader(new File(stopsFile));
			BufferedReader bf= new BufferedReader(fr);
			String linea = bf.readLine();
			linea= bf.readLine();
			
			while(linea!=null){
				String[] datos= linea.split(SEPARADOR,10);
				
				int numero;
				double numero2;
				if(datos[1].equals(" ")){
					numero=0;
				}
				else numero=Integer.parseInt(datos[1]);
				System.out.println(Integer.parseInt(datos[0])+"," +numero+","+ datos[2]+","+datos[3]+ ","+Double.parseDouble(datos[4])+","+Double.parseDouble(datos[5])+","+ datos[6]+","+ datos[7]+","+datos[8]+","+datos[9]);
				
				VOStop stop= new VOStop(Integer.parseInt(datos[0]), numero, datos[2], datos[3], Double.parseDouble(datos[4]), Double.parseDouble(datos[5]), datos[6], datos[7], Integer.parseInt(datos[8]), datos[9]);
			    stops.put(stop.darStopId(), stop);          
			
				
				
				linea=bf.readLine();
			}
			bf.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	
	

	@Override
	public IList<VORoute> routeAtStop(String stopName) {
//		// TODO Auto-generated method stub
//		DoubleLinkedList<VORoute> rutas = new DoubleLinkedList<VORoute>();
//		
//		VOStop actual = (VOStop) stops.first();
//		int contador = 0;
//		while(contador<stops.getSize())
//		{
//			if(actual.darStopName().equals(stopName))
//			{
//				VOStopTimes actualtimes = (VOStopTimes) stopTimes.first();
//				int contador2=0;
//				while(contador2<stopTimes.getSize())
//				if(actualtimes.getStop_id()==actual.darStopId())
//				{
//					
//				}
//			}
//		}

		return null;
		
		
	}

	
	
	@Override
	public void readBusUpdate(File rtFile){
		// TODO Auto-generated method stub
		
			  actualizaciones = (IQueue<BusUpdateVO>) new IQueue<BusUpdateVO>();
			  Gson gson = new Gson();
		        BufferedReader reader = null;
		        try {
		            
		            BusUpdateVO[] newUpdates = gson.fromJson(new FileReader(rtFile),  BusUpdateVO[].class);
		            for(int i=0;i<newUpdates.length;i++)
		            {
		            	actualizaciones.enqueue( newUpdates[i]);
		            }
		     
		        }
		        catch(Exception e)
		        {
		        	e.printStackTrace();
		        }
		        
	}

	
	
	
	


}
