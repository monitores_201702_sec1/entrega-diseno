package model.vo;

import java.util.Date;

public class VOCalendarDates {
	
	private Number serviceID;
	private Date date;
	private String exceptionType;
	
	public VOCalendarDates(Number ID, Date pFecha, String pTipo)
	{
		serviceID= ID;
		date= pFecha;
		exceptionType = pTipo;
	}
	
	public Number darServiceID()
	{
		return serviceID;
	}
	
	public Date darFecha()
	{
		return date;
	}
	
	public String darExceptionType()
	{
		return exceptionType;
	}
}
