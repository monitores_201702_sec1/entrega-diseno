package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop {

	private int 	stop_id;
	private int stop_code;
	private String stop_name;
	private String stop_desc;
	private double stop_lat;
	private double stop_lon;
	private String zone_id;
	private String stop_url;
	private int location_type;
	private String parent_station;
	

	public VOStop(int pStopId, int pStopCode, String pStopName, String pStopDesc, double pStopLat, double pStopLon, String pZoneId, String pStopUrl,int pLocationType, String pParentSation){
		
		stop_id=pStopId;
		stop_code=pStopCode;
		stop_name=pStopName;
		stop_desc=pStopDesc;
		stop_lat=pStopLat;
		stop_lon=pStopLon;
		zone_id=pZoneId;
		stop_url=pStopUrl;
		location_type=pLocationType;
		parent_station=pParentSation;
	}
	/**
	 * @return id - stop's id
	 */
	public int darStopId() {
		
		return stop_id;
	}
	
	public int darStopCode(){
		
		return stop_code;
	}
	
	/**
	 * @return name - stop name
	 */
	public String darStopName() {
	
		return stop_name;
	}
	
	public String darStopDesc(){
		return stop_desc;
	}
	public double darStopLat(){
		return stop_lat;
	}
	public double darStopLon(){
		return stop_lon;
	}
	public String darZondeId(){
		return zone_id;
	}
	public String darStopUrl(){
		return stop_url;
	}
	public int darLocationType(){
		return location_type;
	}
	public String darParentStation(){
		return parent_station;
	}

}
