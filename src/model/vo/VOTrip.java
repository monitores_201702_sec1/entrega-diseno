package model.vo;

import model.data_structures.HashTableLP;

public class VOTrip {

	private int route_id;
	private int service_id;
	private int trip_id;
	private String trip_headsign;
	private String trip_short_name; 
	private int direction_id; 
	private int block_id;
	private int shape_id;
	private int wheelchair_accessible; 
	private int bikes_allowed;
	private HashTableLP<Integer,VOStop> stops;

	public VOTrip(int pRoute_id, int pService_id, int pTrip_id, String pTrip_headsing, String pTrip_short_name, int pDirection_id, int pBlock_id,int pShape_id, int pWheelchair_accessible,int pBikes_allowed){

		route_id=pRoute_id;
		service_id= pService_id;
		trip_id=pTrip_id;
		trip_headsign=pTrip_headsing;
		trip_short_name=pTrip_short_name;
		direction_id=pDirection_id;
		block_id=pBlock_id;
		shape_id= pShape_id;
		wheelchair_accessible=pWheelchair_accessible;
		bikes_allowed=pBikes_allowed;
		stops= new HashTableLP<>();
	}
	

	public int getRouteID()
	{
		return route_id;
	}
	
	public int getServiceID()
	{
		return service_id;
	}
	
	public int tripID()
	{
		return trip_id;
	}
	
	public String getTripHeadSign()
	{
		return trip_headsign;
	}
	
	public String getTripShortName()
	{
		return trip_short_name;
	}
	public int getDirectionID()
	{
		return direction_id;
	}
	
	public int getBlockID()
	{
		return block_id;
	}
	
	public int getShapeID()
	{
		return shape_id;
	}
	
	public int Wheelchair()
	{
		return wheelchair_accessible;
	}
	public int bikes()
	{
		return bikes_allowed;
	}
	
	public int darRouteId(){
		return route_id;
	}
	public int darServiceId(){
		return service_id;
	}
	public int darTripId(){
		return trip_id;
	}
	public String darTripHeadSign(){
		return trip_headsign;
	}
	
	public void agregarParada(VOStop stop)
	{
		stops.put(stop.darStopId(), stop);
	}
	
	public VOStop darParada(int pStopID)
	{
		return stops.get(pStopID);
	}
	
	public HashTableLP<Integer, VOStop> darParadas()
	{
		return stops;
	}
}
