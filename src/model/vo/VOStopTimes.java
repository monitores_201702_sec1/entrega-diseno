package model.vo;

public class VOStopTimes {

	private Number trip_id;
	private String arrival_time;
	private String departure_time;
	private Number stop_id;
	private Number stop_sequence;
	private Number stop_headsign;
	private Number pickup_type;
	private Number drop_off_type;
	private Number shape_dist_traveled;

public VOStopTimes(Number pTrip_id,String pArrival_time,String pDeparture_time,Number pStop_id,Number pStop_sequence,Number pStop_headsign,Number pPickup_type,Number pDrop_off_type,Number pShape_dist_traveled){
	trip_id=pTrip_id;
	arrival_time=pArrival_time;
	departure_time=pDeparture_time;
	stop_id=pStop_id;
	stop_sequence=pStop_sequence;
	stop_headsign=pStop_headsign;
	pickup_type=pPickup_type;
	drop_off_type=pDrop_off_type;
	shape_dist_traveled=pShape_dist_traveled;
}
public Number getTrip_id() {
	return trip_id;
}

public String getArrival_time() {
	return arrival_time;
}


public String getDeparture_time() {
	return departure_time;
}

public Number getStop_id() {
	return stop_id;
}


public Number getStop_sequence() {
	return stop_sequence;
}


public Number getStop_headsign() {
	return stop_headsign;
}


public Number getPickup_type() {
	return pickup_type;
}



public Number getDrop_off_type() {
	return drop_off_type;
}



public Number getShape_dist_traveled() {
	return shape_dist_traveled;
}


}
