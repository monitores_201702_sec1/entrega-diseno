package model.vo;

public class VOFeedInfo {

	private String feed_publisher_name;
	private String feed_publisher_url;
	private String feed_lang;
	private String feed_start_date;
	private String feed_end_date;
	private String feed_version;
	
	public VOFeedInfo(String pfeed_publisher_name, String pfeed_publisher_url, String pfeed_lang, String pfeed_start_date, String pfeed_end_date,String pfeed_version){
		feed_publisher_name=pfeed_publisher_name;
		feed_publisher_url=pfeed_publisher_url;
		feed_lang=pfeed_lang;
		feed_start_date=pfeed_start_date;
		feed_end_date= pfeed_end_date;
		feed_version=pfeed_version;
	}
	
	public String darFeedPublisherName(){
		return feed_publisher_name;
	}
	public String darFeedPublisherUrl(){
		return feed_publisher_url;
	}
	public String darFeedLang(){
		return feed_lang;
	}
	public String darFeedStartDate(){
		return feed_start_date;
	}
	public String darFeedEndDate(){
		return feed_end_date;
	}
	public String darFeedVersion(){
		return feed_version;
	}
	
}
