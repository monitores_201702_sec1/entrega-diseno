package model.vo;

public class VOShapes {

	private Number shape_id;
	private Number shape_pt_lat;
	private Number shape_pt_lon;
	private Number shape_pt_sequence;
	private Number shape_dist_traveled;
	
	public VOShapes(Number pShapeId, Number pShapePtLat, Number pShapePtLon, Number pShapePtSequence, Number pShapeDistTraveled){
		
		 shape_id=pShapeId;
		 shape_pt_lat=pShapePtLat;
		 shape_pt_lon= pShapePtLon;
		 shape_pt_sequence= pShapePtSequence;
		 shape_dist_traveled= pShapeDistTraveled;
	}
	
	public Number darShapeId(){
		return shape_id;
	}
	
	public Number darShapePtLat(){
	return shape_pt_lat;	
	}
	
	public Number darShapePtLon(){
		return shape_pt_lon;
	}
	
	public Number darShapePtSequence(){
		return shape_pt_sequence;
	}
	public Number darShapeDistTraveled(){
		return shape_dist_traveled;
	}
}
