package model.vo;

public class BusUpdateVO {
private String VehicleNo;
	
	private Number TripId;
	
	private String RouteNo;
	
	private String Direction;
	
	private String Destination;
	
	private String Pattern;
	
	private Number Latitude;
	
	private Number Longitude;
	
	private String RecordedTime;

	private VORouteMap RouteMap;
	
//	public BusUpdateVO(String pVehicleNo, Number pTripId, String pRouteNo, String pDestination, String pPattern, Number pLatitude, Number pLongitude, String pRecordedTime, VORouteMap pRouteMap )
//	{
//		VehicleNo= pVehicleNo;
//		TripId = pTripId;
//		
//	}
	
	public String darvehicleNo()
	{
		return VehicleNo;
	}
	
	public Number darTripId()
	{
		return TripId;
	}
	
	public String darRouteNo()
	{
		return RouteNo;
	}
	
	public  String darDirection() 
	{
		return Direction;
	}
	
	public  String darDestination() 
	{
		return Destination;
	}
	
	public String darPattern()
	{
		return Pattern;
	}
	
	public Number darLatitude()
	{
		return Latitude;
	}
	
	public Number darlongitude()
	{
		return Longitude;
	}
	
	public String darRecordedtime()
	{
		return RecordedTime;
	}
	
	public VORouteMap darRouteMap()
	{
		return RouteMap;
	}
	
	

}
