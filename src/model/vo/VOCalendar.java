package model.vo;

import java.util.Date;

public class VOCalendar {

	private boolean monday;
	private boolean tuesday;
	private boolean wednesday;
	private boolean thursday;
	private boolean friday;
	private boolean saturday;
	private boolean sunday;
	private Number serviceID;
	private Date start_date;
	private Date end_date;

	public VOCalendar(boolean pM, boolean pT, boolean pW, boolean pTH, boolean F, boolean Sa, boolean Sun, Number ID, Date start, Date end)
	{
		monday= pM;
		tuesday= pT;
		wednesday=pW;
		thursday=pTH;
		friday=F;
		saturday=Sa;
		sunday= Sun;
		serviceID=ID;
		start_date=start;
		end_date=end;
	}

	
	public boolean darMonday()
	{
		return monday;
	}
	
	public boolean darTuesday()
	{
		return tuesday;
	}

	public boolean darWednesday()
	{
		return wednesday;
	}
	
	public boolean darThursday()
	{
		return thursday;
	}
	
	public boolean darFriday()
	{
		return friday;
	}
	
	public Number darServiceID()
	{
		return serviceID;
	}
	
	public Date darStartDate()
	{
		return start_date;
	}
	
	public Date darEndDate()
	{
		return end_date;
	}
}
