 package model.vo;

import model.data_structures.IList;

public class VORoute<T> {
	
	private Number route_id;
	private String agency_id;
	private String route_short_name;
	private String route_long_name;
	private String route_desc;
	private Number route_type;
	private String route_url;
	private String route_color;
	private String route_text_color;
	private IList<T> listaDeviajes;
	
	public VORoute(Number pRoute_id,String pAgency_id, String pRoute_short_name , String pRoute_long_name, String pRoute_desc,Number pRoute_type,String pRoute_url, String pRoute_Color, String pRoute_text_color ){
		
		route_id=pRoute_id;
		agency_id=pAgency_id;
		route_short_name=pRoute_short_name;
		route_long_name=pRoute_long_name;
		route_desc=pRoute_desc;
		route_type=pRoute_type;
		route_url=pRoute_url;
		route_color=pRoute_Color;
		route_text_color=pRoute_text_color;
		listaDeviajes= null;
		
	}
	
	public Number darRouteId(){
		return route_id;
	}
	public String darAgencyId(){
		return agency_id;
	}
	public String darRouteShortName(){
		return route_short_name;
	}
	public String darRouteLongName(){
		return route_long_name;
	}
	public String darRouteDesc(){
		return route_desc;
	}
	public Number darRouteType(){
		return route_type;
	}
	public String darRouteUrl(){
		return route_url;
	}
	public String darRouteColor(){
		return route_color;
	}
	public String darRouteTexColor(){
		return route_text_color;
	}
	
	public IList<T> darListaDeViajes(){
		return listaDeviajes;
	}
	public void agregarViajes(IList pLista){
		listaDeviajes=pLista;
	}

}
