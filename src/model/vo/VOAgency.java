package model.vo;

import model.data_structures.HashTableLP;

public class VOAgency {
	
	private String agency_id;
	private String agency_name;
	private String agency_url; 
	private String agency_timezone;
	private String agency_lang;
	private HashTableLP<Integer, VOTrip> trips;
	public VOAgency(String pAgencyId, String pAgencyName, String pAgencyUrl, String pAgencyTimeZone, String pAgencyLang){
		agency_id=pAgencyId;
		agency_lang= pAgencyLang;
		agency_name=pAgencyName;
		agency_timezone= pAgencyTimeZone;
		agency_url=pAgencyUrl;
		trips= new HashTableLP<>();
		
	}

}
