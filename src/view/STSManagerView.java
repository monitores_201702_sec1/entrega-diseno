package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.IList;
import model.vo.VORoute;
import model.vo.VOStop;

public class STSManagerView {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					Controller.loadRoutes();
					break;
				case 2:
					Controller.loadTrips();
					break;
				case 3:
					Controller.loadStopTimes();
					break;
				case 4:
					Controller.loadStops();
					break;
				case 5:
				Controller.loadAgencys();
				case 6:
				Controller.readBusUpdates();
				case 7:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 2----------------------");
		System.out.println("1. Cree una nueva coleccion de rutas (data/routes.txt)");
		System.out.println("2. Cree una nueva coleccion de viajes (data/trips.txt)");
		System.out.println("3. Cree una nueva coleccion de rutas reales (data/stop_times.txt)");
		System.out.println("4. Cree una nueva coleccion de paradas (data/stops.txt)");
		System.out.println("5. Cargar actualizaciones de buses");
		System.out.println("7. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
		
	}
}
