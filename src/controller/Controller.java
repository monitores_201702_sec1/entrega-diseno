package controller;

import java.io.File;

import api.ISTSManager;
import model.data_structures.IList;
import model.logic.STSManager;
import model.vo.VORoute;
import model.vo.VOStop;

public class Controller {

	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();
	
	public static void loadRoutes() {
		manager.loadRoutes("data/routes.txt");
		
	}
		
	public static void loadTrips() {
		manager.loadTrips("data/trips.txt");
		
	}

	public static void loadStopTimes() {
		manager.loadStopTimes("data/stop_times.txt");
	}
	
	public static void loadStops() {
		manager.loadStops("data/stops.txt");
		
	}
	
	public static void loadAgencys()
	{
		manager.loadAgencys("data/agency.txt");
	}
	
	public static void readBusUpdates() 
	{
		File f = new File("data/RealTime-8-21-BUSES_SERVICE");
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			manager.readBusUpdate(updateFiles[i]);
		}
	}
}
